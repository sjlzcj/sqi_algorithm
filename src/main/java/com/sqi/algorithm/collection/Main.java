package com.sqi.algorithm.collection;

import java.util.ArrayList;
import java.util.List;

/**
 * @author songjinlong
 * @version V1.0.0
 * @date 2021/11/22
 **/
public class Main {
    public static void main(String[] args) {
        Object obj = 100;
        Object obj1 = "200";
        List<Object> list = new ArrayList<>();
        list.add(1L);
        list.add("2");
        list.add(obj);
        list.add(obj1);

        for (int i = 0; i < list.size(); i++) {
            System.out.println(Long.valueOf(list.get(i).toString()));
        }
    }
}
