package com.sqi.algorithm.structure;

/**
 * @author songjinlong
 * @version V1.0.0
 * @date 2021/12/21
 **/
public class LinkedTest {

    public static void main(String[] args) {
        Node head = new Node(0);
        Node curr = head;
        for (int i = 1; i < 10; i++) {
            curr.next = new Node(i);
            curr = curr.next;
        }
        curr = head;
        while (curr != null) {
            System.out.println(curr.value);
            curr = curr.next;
        }
        curr = reversal(head);
        while (curr != null) {
            System.out.println(curr.value);
            curr = curr.next;
        }
    }

    private static Node reversal(Node head) {
        if (head == null || head.next == null) {
            return head;
        }
        Node nowHead = reversal(head.next);
        head.next.next = head;
        head.next = null;
        return nowHead;
    }

    private static class Node {
        private Node next;
        private final int value;

        public Node(int value) {
            this.value = value;
        }
    }
}


